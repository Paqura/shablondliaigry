import Mouse from './js/mouse';
import Ball from './js/ball';

const canvas = document.querySelector('.jelly');
const ctx = canvas.getContext('2d');
const mouse = new Mouse(canvas);
const balls = [];
const width = window.innerWidth;
const height = window.innerHeight

canvas.width = width;
canvas.height = height;

for (let i = 0; i < 100; i++) {
  balls.push(new Ball(
    ctx,
    Math.random() * width, 
    Math.random() * height)
  );
}

function render() {
  requestAnimationFrame(render);
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  mouse.setPos(mouse.x, mouse.y, ctx);

  balls.forEach(ball => {
    ball.draw(ctx);
    ball.think(mouse);
  });
}

render();
