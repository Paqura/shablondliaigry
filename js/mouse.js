export default class Mouse {
  constructor(canvas) {
    let rect = canvas.getBoundingClientRect();
    this.x = 40;
    this.y = 0;
    this.width = 20;
    this.height = 80;
    this.color = 'green';
    canvas.addEventListener('mousemove', evt => {
      this.x = evt.clientX - rect.left;
      this.y = evt.clientY - rect.top;
    });
  }
  setPos(x, y, ctx) {
    this.x = x;
    this.y = y;
    this.draw(ctx);
  }
  draw(ctx) {
    ctx.rect(this.x - (this.width/2), this.y - (this.height/2), this.width, this.height);
    ctx.fillStyle = this.color;
    ctx.fill();
  }
}