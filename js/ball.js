export default class Ball {
  constructor(ctx, x = 0, y = 0, r = 5, color = '#ff6600') {
    this.x = x;
    this.y = y;
    this.vx = 0;
    this.vy = 0;
    this.r = r;
    this.color = color;
    this.friction = 0.989;
  }

  think(mouse) {
    const dx = Math.abs(this.x - mouse.x);
    const dy = Math.abs(this.y - mouse.y);
    const distanseX = mouse.width/2;
    const distanseY = mouse.height/2;
    
    if(dx <= distanseX && dy <= distanseY) {
      const angle = Math.atan2(dx, dy);
      const tx = mouse.x + Math.cos(angle) * mouse.width;
      const ty = mouse.x + Math.cos(angle) * (mouse.width * 0.5);

      this.vx += tx - this.x;
      this.vy += ty - this.x;
    }

    this.vx *= this.friction;
    this.vy *= this.friction;

    this.x += this.vx;
    this.y += this.vy;
  }

  draw(ctx) {
    ctx.save();
    ctx.beginPath();
    ctx.arc(this.x, this.y, this.r, 0, 2 * Math.PI);
    ctx.fillStyle = this.color;
    ctx.fill();
    ctx.closePath();
    ctx.restore();
  }
}